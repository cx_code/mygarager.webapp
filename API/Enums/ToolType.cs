﻿namespace API.Enums
{
    public enum ToolType
    {
        Wrench,
        Extension,
        Socket,
        Screwdriver,
        Ratchet,

        Other
    }
}
