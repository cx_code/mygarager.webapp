﻿using System;


namespace API.Constants
{
    public static class ConversionConstants
    {
        public static float HorsePowerToKiloWatts
        {
            get => 1 / 0.745699872F;
        }


        public static float KiloWattsToHorsePower
        {
            get => 0.745699872F;
        }
    }
}
