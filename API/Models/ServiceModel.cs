﻿using System;


namespace API.Models
{
    public class ServiceModel
    {
        public int ID
        { get; set; }

        public DateTime StartDate
        { get; set; }

        public DateTime FinishDate
        { get; set; }

        public int CarID
        { get; set; }

        public string OwnerName
        { get; set; }

        public string OwnerSurname
        { get; set; }

        public int MechanicID
        { get; set; }

        public decimal Cost
        { get; set; }

        public string Note
        { get; set; }
    }
}
