﻿using System;


namespace API.Models
{
    public class ToolModel
    {
        public int ID
        { get; set; }

        public Enums.ToolType Type
        { get; set; }

        public decimal Prize
        { get; set; }

        public int TimesUsed
        { get; set; }

        public DateTime BoughtDate
        { get; set; }

        public string Brand
        { get; set; }

        public string Note
        { get; set; }
    }
}
