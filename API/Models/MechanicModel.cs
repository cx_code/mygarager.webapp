﻿using System;


namespace API.Models
{
    public class MechanicModel
    {
        public int ID
        { get; set; }

        public string Name
        { get; set; }

        public string Surname
        { get; set; }

        public DateTime EmploymentDate
        { get; set; }

        public string BIO
        { get; set; }

        public decimal SalaryGross
        { get; set; }

        public int NumberOfSuccessfulServices
        { get; set; }

        public int NumberOfServices
        { get; set; }

        public string Note
        { get; set; }
    }
}
