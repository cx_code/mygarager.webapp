﻿using System;


namespace API.Models
{
    public class EmployeeModel
    {
        public int ID
        { get; set; }

        public string Name
        { get; set; }

        public string Surname
        { get; set; }

        public DateTime EmploymentDate
        { get; set; }

        public string BIO
        { get; set; }

        public decimal SalaryGross
        { get; set; }

        public string Note
        { get; set; }
    }
}
