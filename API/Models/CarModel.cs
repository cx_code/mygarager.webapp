﻿using System;
using API.Constants;


namespace API.Models
{
    public class CarModel
    {
        public int ID
        { get; set; }

        public string Brand
        { get; set; }

        public string Model
        { get; set; }

        public DateTime ProductionDate
        { get; set; }

        public int CubicCapacity
        { get; set; }

        public int PowerInKW
        { get; set; }

        public int PowerInHP
        {
            get => Convert.ToInt32( PowerInKW * ConversionConstants.KiloWattsToHorsePower );
            set
            {
                if( value > 0 )
                    PowerInKW = Convert.ToInt32( value * ConversionConstants.HorsePowerToKiloWatts );
            }
        }

        public string VIN
        { get; set; }

        public string RegistrationNumber
        { get; set; }

        public int NumberOfVisits
        { get; set; }

        public DateTime LastVisitDate
        { get; set; }

        public string Note
        { get; set; }
    }
}
