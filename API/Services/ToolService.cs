﻿using API.Enums;
using API.Models;
using API.Services.Interfaces;


namespace API.Services
{
    public class ToolService : IToolService
    {
        public void AddNewTool( ToolModel toolToAdd )
        {
            throw new System.NotImplementedException();
        }


        public void OrderNewToolsOfType( ToolType typeToOrder )
        {
            throw new System.NotImplementedException();
        }


        public void RemoveTool( int id )
        {
            throw new System.NotImplementedException();
        }


        public void RemoveTool( ToolModel toolToRemove )
        {
            throw new System.NotImplementedException();
        }


        public void UpdateTool( int id, ToolModel toolToUpdate )
        {
            throw new System.NotImplementedException();
        }
    }
}
