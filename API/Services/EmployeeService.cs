﻿using System;
using API.Models;
using API.Services.Interfaces;


namespace API.Services
{
    public class EmployeeService : IEmployeeService
    {
        public void HireNewEmployee( EmployeeModel employeeToHire )
        {
            throw new NotImplementedException();
        }


        public bool IsHired( EmployeeModel employeeToCheck )
        {
            throw new NotImplementedException();
        }


        public void MakePayment( int employeeID )
        {
            throw new NotImplementedException();
        }


        public void RemoveEmployee( EmployeeModel employeeToRemove )
        {
            throw new NotImplementedException();
        }


        public void UpdateEmployee( int employeeID, EmployeeModel employeeToUpdate )
        {
            throw new NotImplementedException();
        }
    }
}
