﻿using System;
using API.Models;


namespace API.Services.Interfaces
{
    public interface IEmployeeService
    {
        public void HireNewEmployee( EmployeeModel employeeToHire );

        public void RemoveEmployee( EmployeeModel employeeToRemove );

        public void UpdateEmployee( int employeeID, EmployeeModel employeeToUpdate );

        public void MakePayment( int employeeID );

        public bool IsHired( EmployeeModel employeeToCheck );
    }
}
