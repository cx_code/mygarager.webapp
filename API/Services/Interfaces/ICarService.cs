﻿using API.Models;


namespace API.Services.Interfaces
{
    public interface ICarService
    {
        public void RegisterNewCar( CarModel carToRegister );

        public void UpdateCar( int currentID, CarModel updatedCar );

        public bool IsCarRegitered( CarModel car );

        public bool IsCarStolen( string VIN );

        public void DeleteCar( CarModel carToDelete );
    }
}
