﻿using System;
using API.Models;


namespace API.Services.Interfaces
{
    public interface IToolService
    {
        public void AddNewTool( ToolModel toolToAdd );

        public void RemoveTool( int id );

        public void RemoveTool( ToolModel toolToRemove );

        public void OrderNewToolsOfType( Enums.ToolType typeToOrder );

        public void UpdateTool( int id, ToolModel toolToUpdate );
    }
}
