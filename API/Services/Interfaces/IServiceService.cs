﻿using System;
using API.Models;


namespace API.Services.Interfaces
{
    public interface IServiceService
    {
        public void PrintInvoiceForService( int serviceID );

        public void SetEndDate( DateTime endDate );

        public void SetFirstMechanic( int mechanicID );

        public bool IsFinished();

        public void SetStartDate( DateTime startDate );

        public void RegisterNewService( ServiceModel serviceToRegister );

        public void DeleteService( ServiceModel serviceToDelete );

        public void UpdateService( int serviceID, ServiceModel serviceToUpdate );

        public void IsServiceRegistered( ServiceModel service );
    }
}
