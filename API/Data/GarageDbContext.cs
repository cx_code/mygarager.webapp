﻿using Microsoft.EntityFrameworkCore;
using API.Models;
using System;
using Microsoft.Extensions.DependencyInjection;


namespace API.Data
{
    public class GarageDbContext : DbContext
    {
        public GarageDbContext( DbContextOptions options ) : base( options )
        {
        }


        protected override void OnModelCreating( ModelBuilder builder )
        {
            builder.Entity<ToolModel>().HasKey( model => model.ID );
            builder.Entity<ServiceModel>().HasKey( model => model.ID );
            builder.Entity<MechanicModel>().HasKey( model => model.ID );
            builder.Entity<EmployeeModel>().HasKey( model => model.ID );
            builder.Entity<CarModel>().HasKey( model => model.ID );
        }


        public DbSet<ToolModel> Tools
        { get; set; }

        public DbSet<ServiceModel> Services
        { get; set; }

        public DbSet<MechanicModel> Mechanics
        { get; set; }

        public DbSet<EmployeeModel> Employees
        { get; set; }

        public DbSet<CarModel> Cars
        { get; set; }


        public static void EnsureCreated( IServiceProvider serviceProvider )
        {
            serviceProvider.GetRequiredService<IServiceScopeFactory>()
                .CreateScope()
                .ServiceProvider.GetRequiredService<GarageDbContext>()
                .Database.EnsureCreated();
        }
    }
}
