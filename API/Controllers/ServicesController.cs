﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using API.Models;
using API.Data;


namespace API.Controllers
{
    [Route( "api/services" )]
    [ApiController]
    public class ServicesController : Controller
    {
        public ServicesController( GarageDbContext dbContext )
        {
            this.dbContext = dbContext;
        }


        [HttpGet( "{id}" )]
        public ServiceModel Get( int id )
        {
            throw new NotImplementedException();
        }


        [HttpGet]
        public List<ServiceModel> Get()
        {
            throw new NotImplementedException();
        }


        [HttpPost]
        public void Post( ServiceModel service )
        {
            throw new NotImplementedException();
        }


        [HttpPut( "{id}" )]
        public void Put( int id, ServiceModel service )
        {
            throw new NotImplementedException();
        }


        [HttpDelete( "{id}" )]
        public void Delete( int id )
        {
            throw new NotImplementedException();
        }



        private GarageDbContext dbContext;
    }
}
