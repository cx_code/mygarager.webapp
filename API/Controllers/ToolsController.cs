﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using API.Models;
using API.Data;


namespace API.Controllers
{
    [Route("api/tools")]
    [ApiController]
    public class ToolsController : Controller
    {
        public ToolsController( GarageDbContext dbContext )
        {
            this.dbContext = dbContext;
        }


        [HttpGet( "{id}" )]
        public ToolModel Get( int id )
        {
            throw new NotImplementedException();
        }


        [HttpGet]
        public List<ToolModel> Get()
        {
            throw new NotImplementedException();
        }


        [HttpPost]
        public void Post( ToolModel tool )
        {
            throw new NotImplementedException();
        }


        [HttpPut( "{id}" )]
        public void Put( int id, ToolModel tool )
        {
            throw new NotImplementedException();
        }


        [HttpDelete( "{id}" )]
        public void Delete( int id )
        {
            throw new NotImplementedException();
        }



        private GarageDbContext dbContext;
    }
}
