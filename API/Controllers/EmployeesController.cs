﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using API.Models;
using API.Data;


namespace API.Controllers
{
    [Route( "api/employees" )]
    [ApiController]
    public class EmployeesController : Controller
    {
        public EmployeesController( GarageDbContext dbContext )
        {
            this.dbContext = dbContext;
        }


        [HttpGet( "{id}" )]
        public EmployeeModel Get( int id )
        {
            throw new NotImplementedException();
        }


        [HttpGet]
        public List<EmployeeModel> Get()
        {
            throw new NotImplementedException();
        }


        [HttpPost]
        public void Post( EmployeeModel employee )
        {
            throw new NotImplementedException();
        }


        [HttpPut( "{id}" )]
        public void Put( int id, EmployeeModel employee )
        {
            throw new NotImplementedException();
        }


        [HttpDelete( "{id}" )]
        public void Delete( int id )
        {
            throw new NotImplementedException();
        }



        private GarageDbContext dbContext;
    }
}
