﻿using System;
using API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using API.Data;


namespace API.Controllers
{
    [Route("api/mechanics")]
    [ApiController]
    public class MechanicsController : Controller
    {
        public MechanicsController( GarageDbContext dbContext )
        {
            this.dbContext = dbContext;
        }


        [HttpGet( "{id}" )]
        public MechanicModel Get( int id )
        {
            throw new NotImplementedException();
        }


        [HttpGet]
        public List<MechanicModel> Get()
        {
            throw new NotImplementedException();
        }


        [HttpPost]
        public void Post( MechanicModel mechanic )
        {
            throw new NotImplementedException();
        }


        [HttpPut( "{id}" )]
        public void Put( int id, MechanicModel mechanic )
        {
            throw new NotImplementedException();
        }


        [HttpDelete( "{id}" )]
        public void Delete( int id )
        {
            throw new NotImplementedException();
        }



        private GarageDbContext dbContext;
    }
}
