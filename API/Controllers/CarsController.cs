﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using API.Models;
using API.Data;


namespace API.Controllers
{
    [Route("/api/cars")]
    [ApiController]
    public class CarsController : Controller
    {
        public CarsController( GarageDbContext dbContext )
        {
            this.dbContext = dbContext;
        }


        [HttpGet( "{id}" )]
        public CarModel Get( int id )
        {
            throw new NotImplementedException();
        }


        [HttpGet]
        public List<CarModel> Get()
        {
            throw new NotImplementedException();
        }


        [HttpPost]
        public void Post( CarModel car )
        {
            throw new NotImplementedException();
        }


        [HttpPut( "{id}" )]
        public void Put( int id, CarModel car )
        {
            throw new NotImplementedException();
        }


        [HttpDelete( "{id}" )]
        public void Delete( int id )
        {
            throw new NotImplementedException();
        }



        private GarageDbContext dbContext;
    }
}
